﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        public bool isSet = false;

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (!radioButtonEverything.Checked && !radioButtonImportant.Checked && !radioButtonToDo.Checked)
                MessageBox.Show("Please choose one category!!!");
            else
            {
                isSet = true;
                this.Close();
            }
        }
    }
}
