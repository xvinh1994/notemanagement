﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net.Mail;

namespace NoteManagement
{
    public partial class SendMail : Form
    {
        public SendMail()
        {
            InitializeComponent();
        }

        private void radioButtonYahoo_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAccount.Enabled = radioButtonYahoo.Checked;
            textBoxPassword.Enabled = radioButtonYahoo.Checked;
            textBoxReceiver.Enabled = radioButtonYahoo.Checked;
            buttonSend.Enabled = radioButtonYahoo.Checked;
        }

        private void radioButtonGmail_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAccount.Enabled = radioButtonGmail.Checked;
            textBoxPassword.Enabled = radioButtonGmail.Checked;
            textBoxReceiver.Enabled = radioButtonGmail.Checked;
            buttonSend.Enabled = radioButtonGmail.Checked;
        }

        private void radioButtonHotmail_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAccount.Enabled = radioButtonHotmail.Checked;
            textBoxPassword.Enabled = radioButtonHotmail.Checked;
            textBoxReceiver.Enabled = radioButtonHotmail.Checked;
            buttonSend.Enabled = radioButtonHotmail.Checked;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            Form1 parent = this.Owner as Form1;

            if (parent.richTextBoxNote.Text != "")
            {
                MessageBox.Show("Note will be sent to " + textBoxReceiver.Text + " with subject: \"My Note Sharing\"");
                MailMessage mail;
                SmtpClient client;

                string dataToSend = "- Tag: [" + parent.config.tagStr + "]\n" + 
                        "- Description: " + parent.Text + "\n" + 
                        "- Data in note: " + parent.richTextBoxNote.Text + "\n" +
                        "- Sending Day: " + DateTime.Now.ToString();

                string server = "";
                string sendUser = "";
                string passwordUser = "";
                string receiveUser = "";

                if (radioButtonYahoo.Checked)
                {
                    server = "smtp.mail.yahoo.com";
                }
                else if (radioButtonGmail.Checked)
                {
                    server = "smtp.gmail.com";
                }
                else if (radioButtonHotmail.Checked)
                {
                    server = "smtp.live.com";
                }

                sendUser = textBoxAccount.Text;
                passwordUser = textBoxPassword.Text;
                receiveUser = textBoxReceiver.Text;

                mail = new MailMessage(sendUser, receiveUser, "My Note Sharing", dataToSend);
                client = new SmtpClient(server);
                client.Port = 587;
                client.Credentials = new System.Net.NetworkCredential(sendUser, passwordUser);
                client.EnableSsl = true;
                try
                {
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else MessageBox.Show("No data in note to send");
            this.Close();
        }
    }
}
