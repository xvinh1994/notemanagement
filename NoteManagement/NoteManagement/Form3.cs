﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        public Form3(List<Form1> list)
        {
            InitializeComponent();
            listNote = list;
            gotNote = new bool[listNote.Count];
        }

        private List<Form1> listNote;
        private bool[] gotNote;

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (textBoxSearch.Text != "")
            {
                string[] strSearchArray = textBoxSearch.Text.Split(' ');
                searchNote(strSearchArray);
                outputResult();
            }
            else
            {
                MessageBox.Show("Please input data to search!!!");
                textBoxSearch.Focus();
            }
        }

        private void outputResult()
        {
			listBoxResult.Items.Clear();
            for (int i = 0; i < gotNote.Length; i++)
            {
                if (gotNote[i] == true)
                {
                    listBoxResult.Items.Add("Note #" + i + " has word(s) you need. Double click to show note.");
                }
            }

            if (listBoxResult.Items.Count == 0) 
			{
				labelNoResult.Visible = true;
				listBoxResult.Visible = false;
			}
            else 
			{
				listBoxResult.Visible = true;
				labelNoResult.Visible = false;
			}
        }

        private void searchNote(string[] strSearchArray)
        {
            for (int i = 0; i < listNote.Count; i++) 
            {
                var note = listNote[i];
                List<string> strDataArray = combineAllToStringArr(note.config.tagStr, note.Text, note.richTextBoxNote.Text);

                foreach (var str in strSearchArray)
                {
                    if (strDataArray.FindIndex(x => x == str) != -1)
                    {
                        gotNote[i] = true;
                        break;
                    }
                }
            }
        }

        private List<string> combineAllToStringArr(string tag, string description, string data)
        {
            List<string> result = new List<string>();

            if (tag != "" || tag != null) 
            {
                string[] tmp1 = tag.Split(' ');

                foreach (var str in tmp1)
                {
                    result.Add(str);
                }
            }

            if (description != "" || description != null)
            {
                string[] tmp2 = description.Split(' ');

                foreach (var str in tmp2)
                {
                    result.Add(str);
                }
            }

            if (data != "" || data != null)
            {
                string[] tmp3 = data.Split(' ');


                foreach (var str in tmp3)
                {
                    result.Add(str);
                }
            }

            return result;
        }

        private void listBoxResult_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxResult.SelectedItem != null)
            {
                string chosen = listBoxResult.SelectedItem.ToString();
                string[] split = chosen.Split('#');
                string[] split2 = split[1].Split(' ');
                int idx;
                int.TryParse(split2[0].ToString(), out idx);

                listNote[idx].Focus();
            }
        }

        private void textBoxSearch_Enter(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            int visibleTime = 1500;

            ToolTip toolTip = new ToolTip();
            toolTip.Show("seperate words by space character", txtBox, 0, 20, visibleTime);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
