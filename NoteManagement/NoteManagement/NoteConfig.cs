﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteManagement
{
    public class NoteConfig
    {
        public string tagStr = "";
        public string description = "";
        public int timeLeft = 0;
        public bool important = false;
        public bool isThisDeleted = false;
        public byte R = 255, B = 255, G = 255;

        public NoteConfig() { }

        public NoteConfig(string tag, string reminder, string important, string red, string blue, string green)
        {
            this.tagStr = tag;
            this.timeLeft = Convert.ToInt32(reminder);
            this.important = Convert.ToBoolean(important);
            this.R = Convert.ToByte(red);
            this.B = Convert.ToByte(blue);
            this.G = Convert.ToByte(green);
        }
    }
}
