﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void checkBoxOnOff_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTime.Enabled = checkBoxOnOff.Checked;
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            Form1 parent = this.Owner as Form1;
            int temp;
            int.TryParse(textBoxTime.Text,out temp);
            parent.config.timeLeft = temp;
            this.Close();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            Form1 parent = this.Owner as Form1;
            if (parent.config.timeLeft != 0)
            {
                checkBoxOnOff.Checked = true;
                int temp = parent.config.timeLeft;
                textBoxTime.Text = temp.ToString();
            }

            textBoxTime.Enabled = checkBoxOnOff.Checked;
        }
    }
}
