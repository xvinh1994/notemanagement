﻿namespace NoteManagement2
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxTag = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.radioButtonToDo = new System.Windows.Forms.RadioButton();
            this.radioButtonImportant = new System.Windows.Forms.RadioButton();
            this.radioButtonEverything = new System.Windows.Forms.RadioButton();
            this.buttonOK = new System.Windows.Forms.Button();
            this.checkBoxReminder = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(25, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tag:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(25, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description:";
            // 
            // textBoxTag
            // 
            this.textBoxTag.Location = new System.Drawing.Point(124, 22);
            this.textBoxTag.Name = "textBoxTag";
            this.textBoxTag.Size = new System.Drawing.Size(236, 20);
            this.textBoxTag.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(25, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Category:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(124, 53);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(236, 20);
            this.textBoxDescription.TabIndex = 4;
            // 
            // radioButtonToDo
            // 
            this.radioButtonToDo.AutoSize = true;
            this.radioButtonToDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonToDo.Location = new System.Drawing.Point(39, 160);
            this.radioButtonToDo.Name = "radioButtonToDo";
            this.radioButtonToDo.Size = new System.Drawing.Size(79, 19);
            this.radioButtonToDo.TabIndex = 5;
            this.radioButtonToDo.TabStop = true;
            this.radioButtonToDo.Text = "To-do List";
            this.radioButtonToDo.UseVisualStyleBackColor = true;
            // 
            // radioButtonImportant
            // 
            this.radioButtonImportant.AutoSize = true;
            this.radioButtonImportant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonImportant.Location = new System.Drawing.Point(158, 160);
            this.radioButtonImportant.Name = "radioButtonImportant";
            this.radioButtonImportant.Size = new System.Drawing.Size(77, 19);
            this.radioButtonImportant.TabIndex = 6;
            this.radioButtonImportant.TabStop = true;
            this.radioButtonImportant.Text = "Important";
            this.radioButtonImportant.UseVisualStyleBackColor = true;
            // 
            // radioButtonEverything
            // 
            this.radioButtonEverything.AutoSize = true;
            this.radioButtonEverything.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonEverything.Location = new System.Drawing.Point(281, 160);
            this.radioButtonEverything.Name = "radioButtonEverything";
            this.radioButtonEverything.Size = new System.Drawing.Size(81, 19);
            this.radioButtonEverything.TabIndex = 7;
            this.radioButtonEverything.TabStop = true;
            this.radioButtonEverything.Text = "Everything";
            this.radioButtonEverything.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(160, 199);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // checkBoxReminder
            // 
            this.checkBoxReminder.AutoSize = true;
            this.checkBoxReminder.Location = new System.Drawing.Point(28, 92);
            this.checkBoxReminder.Name = "checkBoxReminder";
            this.checkBoxReminder.Size = new System.Drawing.Size(88, 17);
            this.checkBoxReminder.TabIndex = 9;
            this.checkBoxReminder.Text = "Set Alarm On";
            this.checkBoxReminder.UseVisualStyleBackColor = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 232);
            this.Controls.Add(this.checkBoxReminder);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.radioButtonEverything);
            this.Controls.Add(this.radioButtonImportant);
            this.Controls.Add(this.radioButtonToDo);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxTag);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form3";
            this.Text = "Info for Note";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonOK;
        internal System.Windows.Forms.RadioButton radioButtonToDo;
        internal System.Windows.Forms.RadioButton radioButtonImportant;
        internal System.Windows.Forms.RadioButton radioButtonEverything;
        internal System.Windows.Forms.TextBox textBoxTag;
        internal System.Windows.Forms.TextBox textBoxDescription;
        internal System.Windows.Forms.CheckBox checkBoxReminder;
    }
}