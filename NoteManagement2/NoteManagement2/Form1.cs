﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace NoteManagement2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<DataNote> todoList = new List<DataNote>();
        List<DataNote> everything = new List<DataNote>();
        List<DataNote> important = new List<DataNote>();

        private void Form1_Load(object sender, EventArgs e)
        {
            treeView.Nodes.Add("To-do-list");
            treeView.Nodes.Add("Everything");
            treeView.Nodes.Add("Important");

            timerCountDown.Start();
            timerAutoSave.Start();

            CommonMethod.loadSavedNotes(ref todoList, ref important, ref everything);
            addSaveNotesToTree();
        }

        private void addSaveNotesToTree()
        {
            for (int i = 0; i < todoList.Count; i++)
            {
                treeView.Nodes[0].Nodes.Add("Note To-do-list #" + (i + 1).ToString());
            }

            for (int i = 0; i < everything.Count; i++)
            {
                treeView.Nodes[1].Nodes.Add("Note Everything #" + (i + 1).ToString());
            }

            for (int i = 0; i < important.Count; i++)
            {
                treeView.Nodes[2].Nodes.Add("Note Important #" + (i + 1).ToString());
            }
        }

        private void toolStripButtonNew_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();

            if (form3.isSet)
            {
                Form2 form2 = new Form2();
                form2.MdiParent = this;

                DataNote data = getDataFromForm(form3);

                addToList(data);
                form2.loadData(data);
                form2.frmData.isNowOpen = true;

                if (form2.frmData.isToDoList)
                    form2.index = todoList.Count - 1;
                else if (form2.frmData.isImportant)
                    form2.index = important.Count - 1;
                else if (form2.frmData.isEverything)
                    form2.index = everything.Count - 1;

                form2.Show();
            }
        }

        private DataNote getDataFromForm(Form3 form3)
        {
            DataNote data = new DataNote();
            data.dataInNote = "";
            data.description = form3.textBoxDescription.Text;
            data.isEverything = form3.radioButtonEverything.Checked;
            data.isImportant = form3.radioButtonImportant.Checked;
            data.isToDoList = form3.radioButtonToDo.Checked;
            data.tag = form3.textBoxTag.Text;
            data.timeLeft = -1;
            data.isAlarmOn = false;

            if (form3.checkBoxReminder.Checked)
            {
                Form4 form4 = new Form4();
                form4.ShowDialog();

                data.isAlarmOn = true;

                DateTime dateSet = form4.dateSet;
                if (dateSet.Day == DateTime.Now.Day && dateSet.Month == DateTime.Now.Month && dateSet.Year == DateTime.Now.Year)
                    data.timeLeft = 0;
                else data.timeLeft = (form4.dateSet - DateTime.Now).Seconds;

                data.timeLeft += (form4.hour - DateTime.Now.Hour) * 3600;
                data.timeLeft += (form4.minute - DateTime.Now.Minute) * 60;
                data.timeLeft += (form4.second - DateTime.Now.Second);
            }

            return data;
        }

        private void addToList(DataNote data)
        {
            if (data.isEverything)
            {
                everything.Add(data);
                treeView.Nodes[1].Nodes.Add("Note Everything #" + everything.Count.ToString());
            }
            else if (data.isImportant)
            {
                important.Add(data);
                treeView.Nodes[2].Nodes.Add("Note Important #" + important.Count.ToString());
            }
            else if (data.isToDoList)
            {
                todoList.Add(data);
                treeView.Nodes[0].Nodes.Add("Note To-do-list #" + todoList.Count.ToString());
            }
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (e.Node.Level != 0)
                {
                    this.treeView.SelectedNode = e.Node;
                    contextMenuStripTree.Show(MousePosition);
                }
            }
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            if (this.treeView.SelectedNode != null)
            {
                string selectedNode = this.treeView.SelectedNode.Text;
                string[] split = selectedNode.Split('#');

                if (split.Length > 1)
                {
                    int indexInList = Convert.ToInt32(split[1]) - 1;
                    string listName = split[0].Trim();

                    Form2 form2 = new Form2();
                    form2.MdiParent = this;

                    Form[] childFormArray = this.MdiChildren;
                    bool isOpened = false;

                    switch (listName)
                    {
                        case "Note Everything":
                            for (int i = 0; i < childFormArray.Length; i++)
                            {
                                Form2 current = childFormArray[i] as Form2;
                                if(current.index == indexInList && current.frmData.isEverything)
                                {
                                    isOpened = true;
                                    current.Focus();
                                    form2.Close();
                                    break;
                                }
                            }

                            if (!isOpened)
                            {
                                form2.loadData(everything[indexInList]);
                                form2.index = indexInList;
                                form2.frmData.isNowOpen = true;
                                form2.Show();
                            }
                            break;
                        case "Note Important":
                            for (int i = 0; i < childFormArray.Length; i++)
                            {
                                Form2 current = childFormArray[i] as Form2;
                                if (current.index == indexInList && current.frmData.isImportant)
                                {
                                    isOpened = true;
                                    current.Focus();
                                    form2.Close();
                                    break;
                                }
                            }

                            if (!isOpened)
                            {
                                form2.loadData(important[indexInList]);
                                form2.index = indexInList;
                                form2.StartPosition = FormStartPosition.CenterScreen;
                                form2.frmData.isNowOpen = true;
                                form2.Show();
                            }
                            break;
                        case "Note To-do-list":
                            for (int i = 0; i < childFormArray.Length; i++)
                            {
                                Form2 current = childFormArray[i] as Form2;
                                if (current.index == indexInList && current.frmData.isToDoList)
                                {
                                    isOpened = true;
                                    current.Focus();
                                    form2.Close();
                                    break;
                                }
                            }

                            if (!isOpened)
                            {
                                form2.loadData(todoList[indexInList]);
                                form2.index = indexInList;
                                form2.frmData.isNowOpen = true;
                                form2.Show();
                            }
                            break;
                    }
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (confirmToDo("Do you want to delete this note?"))
            {
                string selectedNode = this.treeView.SelectedNode.Text;
                string[] split = selectedNode.Split('#');

                if (split.Length > 1)
                {
                    int indexInList = Convert.ToInt32(split[1]) - 1;

                    Form[] childFormArray = this.MdiChildren;
                    int totalNode;
                    switch (split[0].Trim())
                    {
                        case "Note Everything":
                            if (everything[indexInList].isNowOpen)
                            {
                                for (int i = 0; i < childFormArray.Length; i++)
                                {
                                    Form2 current = childFormArray[i] as Form2;
                                    if (current.index == indexInList && current.frmData.isEverything)
                                    {
                                        current.Close();
                                    }
                                    else if (current.index > indexInList && current.frmData.isEverything)
                                    {
                                        current.index--;
                                    }
                                }
                            }

                            totalNode = treeView.Nodes[1].Nodes.Count;
                            for (int i = indexInList + 1; i < totalNode; i++)
                            {
                                treeView.Nodes[1].Nodes[i].Text = "Note Everything #" + i.ToString();
                            }

                            treeView.Nodes[1].Nodes.Remove(this.treeView.SelectedNode);
                            everything.RemoveAt(indexInList);
                            break;
                        case "Note Important":
                            if (important[indexInList].isNowOpen)
                            {
                                for (int i = 0; i < childFormArray.Length; i++)
                                {
                                    Form2 current = childFormArray[i] as Form2;
                                    if (current.index == indexInList && current.frmData.isImportant)
                                    {
                                        current.Close();
                                    }
                                    else if (current.index > indexInList && current.frmData.isImportant)
                                    {
                                        current.index--;
                                    }
                                }
                            }

                            totalNode = treeView.Nodes[2].Nodes.Count;
                            for (int i = indexInList; i < totalNode; i++)
                            {
                                treeView.Nodes[2].Nodes[i].Text = "Note Important #" + i.ToString();
                            }

                            treeView.Nodes[2].Nodes.Remove(this.treeView.SelectedNode);
                            important.RemoveAt(indexInList);
                            break;
                        case "Note To-do-list":
                            if (todoList[indexInList].isNowOpen)
                            {
                                for (int i = 0; i < childFormArray.Length; i++)
                                {
                                    Form2 current = childFormArray[i] as Form2;
                                    if (current.index == indexInList && current.frmData.isToDoList)
                                    {
                                        current.Close();
                                    }
                                    else if (current.index > indexInList && current.frmData.isToDoList)
                                    {
                                        current.index--;
                                    }
                                }
                            }

                            totalNode = treeView.Nodes[0].Nodes.Count;
                            for (int i = indexInList; i < totalNode; i++)
                            {
                                treeView.Nodes[0].Nodes[i].Text = "Note To-do-list " + i.ToString();
                            }

                            treeView.Nodes[0].Nodes.Remove(this.treeView.SelectedNode);
                            todoList.RemoveAt(indexInList);
                            break;
                    }
                }
            }
        }

        private void deleteAllNoteInCatergoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (confirmToDo("Do you want to delete all notes in this category?"))
            {
                string selectedNode = this.treeView.SelectedNode.Text;
                string[] split = selectedNode.Split('#');
                Form[] children = this.MdiChildren;
                switch (split[0].Trim())
                {
                    case "To-do-list":
                    case "Note To-do-list":
                        treeView.Nodes[0].Nodes.Clear();
                        for (int i = 0; i < children.Length; i++)
                        {
                            Form2 current = children[i] as Form2;
                            if (current.frmData.isToDoList)
                                current.Close();
                        }
                        todoList.Clear();
                        break;
                    case "Note Important":
                    case "Important":
                        treeView.Nodes[2].Nodes.Clear();
                        for (int i = 0; i < children.Length; i++)
                        {
                            Form2 current = children[i] as Form2;
                            if (current.frmData.isImportant)
                                current.Close();
                        }
                        important.Clear();
                        break;
                    case "Note Everything":
                    case "Everything":
                        treeView.Nodes[1].Nodes.Clear();
                        for (int i = 0; i < children.Length; i++)
                        {
                            Form2 current = children[i] as Form2;
                            if (current.frmData.isEverything)
                                current.Close();
                        }
                        everything.Clear();
                        break;
                }
            }
        }

        private void deleteAllNoteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (confirmToDo("Do you want to delete all notes?"))
            {
                todoList.Clear();
                everything.Clear();
                important.Clear();

                foreach (var child in this.MdiChildren)
                {
                    child.Close();
                }
            }
        }

        private bool confirmToDo(string msg)
        {
            if (MessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, 
                MessageBoxDefaultButton.Button2)
                == DialogResult.Yes)
                return true;
            return false;
        }

        private void timerAutoSave_Tick(object sender, EventArgs e)
        {
            updateDataFromActiveChildren();
            CommonMethod.writeXMLFile(todoList, important, everything);
        }

        private void timerCountDown_Tick(object sender, EventArgs e)
        {
            foreach (var note in todoList)
            {
                if (note.timeLeft > 0)
                    note.timeLeft--;
                else if (note.timeLeft == 0 && note.isAlarmOn) 
                {
                    if (note.isNowOpen)
                    {
                        foreach (var child in this.MdiChildren)
                        {
                            Form2 current = child as Form2;
                            if (current.frmData.isToDoList && current.frmData.timeLeft == 0)
                                current.Focus();
                        }
                    }

                    loadFormAlert(note);
                    playSoundAlert();
                    note.timeLeft--;
                    MessageBox.Show("TEST");
                }
            }

            foreach (var note in important)
            {
                if (note.timeLeft > 0)
                    note.timeLeft--;
                else if (note.timeLeft == 0 && note.isAlarmOn)
                {
                    if (note.isNowOpen)
                    {
                        foreach (var child in this.MdiChildren)
                        {
                            Form2 current = child as Form2;
                            if (current.frmData.isImportant && current.frmData.timeLeft == 0)
                                current.Focus();
                        }
                    }
                    
                    loadFormAlert(note);
                    playSoundAlert();
                    note.timeLeft--;
                    MessageBox.Show("TEST");
                }
            }

            foreach (var note in everything)
            {
                if (note.timeLeft > 0)
                    note.timeLeft--;
                else if (note.timeLeft == 0)
                {
                    if (note.isNowOpen && note.isAlarmOn)
                    {
                        foreach (var child in this.MdiChildren)
                        {
                            Form2 current = child as Form2;
                            if (current.frmData.isEverything && current.frmData.timeLeft == 0)
                                current.Focus();
                        }
                    }

                    loadFormAlert(note);
                    playSoundAlert();
                    note.timeLeft--;
                    MessageBox.Show("TEST");
                }
            }
        }

        private void loadFormAlert(DataNote data)
        {
            Form2 form2 = new Form2();
            form2.MdiParent = this;
            form2.loadData(data);
            form2.Show();
            form2.Focus();
        }

        private void playSoundAlert()
        {
            SoundPlayer soundPlayer = new SoundPlayer();
            soundPlayer.Stream = Properties.Resources.ringring;
            soundPlayer.Play();
        }

        private void toolStripButtonSaveAllNotes_Click(object sender, EventArgs e)
        {
            updateDataFromActiveChildren();
            CommonMethod.writeXMLFile(todoList, important, everything);
        }

        private void updateDataFromActiveChildren()
        {
            Form[] children = this.MdiChildren;

            foreach (var child in children)
            {
                Form2 current = child as Form2;

                if (current.frmData.isEverything)
                    everything[current.index].dataInNote = current.richTextBox.Text;
                else if (current.frmData.isImportant)
                    important[current.index].dataInNote = current.richTextBox.Text;
                else if (current.frmData.isToDoList)
                    todoList[current.index].dataInNote = current.richTextBox.Text;
            }
        }

        internal void triggerSaveForChild()
        {
            updateDataFromActiveChildren();
            CommonMethod.writeXMLFile(todoList, important, everything);
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            updateDataFromActiveChildren();
            if (todoList.Count > 0 || important.Count > 0 || everything.Count > 0)
            {
                Form5 form5 = new Form5(todoList, important, everything);
                form5.Owner = this;
                form5.Show();
            }
            else
                MessageBox.Show("NO note to search");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            updateDataFromActiveChildren();
            CommonMethod.writeXMLFile(todoList, important, everything);
        }

        private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Feartures:\r\n - Add, save, delete note.\r\n " +
                "- Search notes word by word.\r\n - Share notes by email.\r\n " + 
                "- Set reminder for one note.");
        }

        private void toolStripButtonMailAllNotes_Click(object sender, EventArgs e)
        {
            List<DataNote> dataToSend = new List<DataNote>();

            foreach (var note in todoList)
            {
                dataToSend.Add(note);
            }

            foreach (var note in everything)
            {
                dataToSend.Add(note);
            }

            foreach (var note in important)
            {
                dataToSend.Add(note);
            }

            Form6 form6 = new Form6(dataToSend);
            form6.ShowDialog();
        }
    }
}
