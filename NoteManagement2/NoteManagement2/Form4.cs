﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement2
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        public int hour, minute, second;
        public DateTime dateSet;

        private void buttonSet_Click(object sender, EventArgs e)
        {
            getDataFromInput();

            if (confirmToSet()) 
                this.Close();
        }

        private bool confirmToSet()
        {
            string msg = "You set up alert on " + dateSet.ToShortDateString() + " at " + hour + "h" + minute + "'" + second + "(s)";
            if (MessageBox.Show(msg, "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button2) == DialogResult.OK)
                return true;
            else return false;
        }

        private void getDataFromInput()
        {
            if (textBoxHour.Text == "") hour = 0;
            else
            {
                int.TryParse(textBoxHour.Text, out hour);

                if (hour > 23 || hour < 0)
                {
                    MessageBox.Show("Value of hour must be >= 0 and <= 23. Hour is set to 0.");
                    hour = 0;
                }
            }

            if (textBoxMinute.Text == "") minute = 0;
            else
            {
                int.TryParse(textBoxMinute.Text, out minute);

                if (minute > 59 || minute < 0)
                {
                    MessageBox.Show("Value of minute must be >= 0 and <= 59. Minute is set to 0.");
                    minute = 0;
                }
            }

            if (textBoxSec.Text == "") second = 0;
            else
            {
                int.TryParse(textBoxSec.Text, out second);

                if (second > 59 || second < 0)
                {
                    MessageBox.Show("Value of second must be >=0 and <= 59. Second is set to 0.");
                    second = 0;
                }
            }

            if (dateTimePicker.Value.Day < DateTime.Now.Day && 
                dateTimePicker.Value.Month < DateTime.Now.Month &&
                dateTimePicker.Value.Year < DateTime.Now.Year)
            {
                dateSet = DateTime.Now;
                MessageBox.Show("Value of date time is incorrect. Date time is set to NOW");
            }
            else dateSet = dateTimePicker.Value;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.FromHandle(Properties.Resources.reminder.GetHicon());
        }
    }
}
