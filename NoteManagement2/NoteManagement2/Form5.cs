﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement2
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        public Form5(List<DataNote> todoList, List<DataNote> important, List<DataNote> everything)
        {
            InitializeComponent();

            this.todoList = todoList;
            this.gotNoteTodoList = new bool[todoList.Count];
            this.important = important;
            this.gotNoteImportant = new bool[important.Count];
            this.everything = everything;
            this.gotNoteEverything = new bool[everything.Count];
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.FromHandle(Properties.Resources.search.GetHicon());
        }

        private List<DataNote> todoList;
        private bool[] gotNoteTodoList;
        private List<DataNote> important;
        private bool[] gotNoteImportant;
        private List<DataNote> everything;
        private bool[] gotNoteEverything;

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (textBoxSearch.Text != "")
            {
                string[] strSearchArray = textBoxSearch.Text.Split(' ');
                searchNote(strSearchArray);
                outputResult();
            }
            else
            {
                MessageBox.Show("Please input data to search!!!");
                textBoxSearch.Focus();
            }
        }

        private void outputResult()
        {
            listBoxResult.Items.Clear();
            for (int i = 0; i < gotNoteTodoList.Length; i++)
            {
                if (gotNoteTodoList[i] == true)
                {
                    listBoxResult.Items.Add("Note #" + (i + 1) + " in \"To-do-list\" notes has word(s) you need. Double click to show note.");
                }
            }

            for (int i = 0; i < gotNoteImportant.Length; i++)
            {
                if (gotNoteImportant[i] == true)
                {
                    listBoxResult.Items.Add("Note #" + (i + 1) + " in \"Important\" notes has word(s) you need. Double click to show note.");
                }
            }

            for (int i = 0; i < gotNoteEverything.Length; i++)
            {
                if (gotNoteEverything[i] == true)
                {
                    listBoxResult.Items.Add("Note #" + (i + 1) + " in \"Everything\" notes has word(s) you need. Double click to show note.");
                }
            }

            if (listBoxResult.Items.Count == 0)
            {
                labelNoResult.Visible = true;
                listBoxResult.Visible = false;
            }
            else
            {
                listBoxResult.Visible = true;
                labelNoResult.Visible = false;
            }
        }

        private void searchNote(string[] strSearchArray)
        {
            for (int i = 0; i < todoList.Count; i++)
            {
                var note = todoList[i];
                List<string> strDataArray = combineAllToStringArr(note.tag, note.description, note.dataInNote);

                foreach (var str in strSearchArray)
                {
                    if (strDataArray.FindIndex(x => x == str) != -1)
                    {
                        gotNoteTodoList[i] = true;
                        break;
                    }
                }
            }

            for (int i = 0; i < important.Count; i++)
            {
                var note = important[i];
                List<string> strDataArray = combineAllToStringArr(note.tag, note.description, note.dataInNote);

                foreach (var str in strSearchArray)
                {
                    if (strDataArray.FindIndex(x => x == str) != -1)
                    {
                        gotNoteImportant[i] = true;
                        break;
                    }
                }
            }

            for (int i = 0; i < everything.Count; i++)
            {
                var note = everything[i];
                List<string> strDataArray = combineAllToStringArr(note.tag, note.description, note.dataInNote);

                foreach (var str in strSearchArray)
                {
                    if (strDataArray.FindIndex(x => x == str) != -1)
                    {
                        gotNoteEverything[i] = true;
                        break;
                    }
                }
            }
        }

        private List<string> combineAllToStringArr(string tag, string description, string data)
        {
            List<string> result = new List<string>();

            if (tag != "" || tag != null)
            {
                string[] tmp1 = tag.Split(' ');

                foreach (var str in tmp1)
                {
                    result.Add(str);
                }
            }

            if (description != "" || description != null)
            {
                string[] tmp2 = description.Split(' ');

                foreach (var str in tmp2)
                {
                    result.Add(str);
                }
            }

            if (data != "" || data != null)
            {
                string[] tmp3 = data.Split(' ');


                foreach (var str in tmp3)
                {
                    result.Add(str);
                }
            }

            return result;
        }

        private void listBoxResult_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxResult.SelectedItem != null)
            {
                string chosen = listBoxResult.SelectedItem.ToString();
                string[] splitSharpSymbol = chosen.Split('#');
                string[] splitSpaceForNum = splitSharpSymbol[1].Split(' ');
                int idx = Convert.ToInt32(splitSpaceForNum[0]) - 1;

                string[] splitForListName = chosen.Split('\"');
                string listName = splitForListName[1];

                showFormInParent(idx, listName);
            }
        }

        private void showFormInParent(int idx, string listName)
        {
            Form1 parent = this.Owner as Form1;
            Form[] childFormArray = null;
            bool hasChild = false;
            if (parent.MdiChildren.Length > 0)
            {
                childFormArray = parent.MdiChildren;
                hasChild = true;
            }
            bool isOpened = false;

            switch (listName)
            {
                case "Everything":
                    if (hasChild)
                    {
                        for (int i = 0; i < childFormArray.Length; i++)
                        {
                            Form2 current = childFormArray[i] as Form2;
                            if (current.index == idx && current.frmData.isEverything)
                            {
                                isOpened = true;
                                current.Focus();
                                break;
                            }
                        }
                    }

                    if (!isOpened)
                    {
                        Form2 form2 = new Form2();
                        form2.MdiParent = parent;
                        form2.loadData(everything[idx]);
                        form2.index = idx;
                        form2.frmData.isNowOpen = true;
                        form2.Show();
                    }
                    break;
                case "Important":
                    if (hasChild)
                    {
                        for (int i = 0; i < childFormArray.Length; i++)
                        {
                            Form2 current = childFormArray[i] as Form2;
                            if (current.index == idx && current.frmData.isImportant)
                            {
                                isOpened = true;
                                current.Focus();
                                break;
                            }
                        }
                    }

                    if (!isOpened)
                    {
                        Form2 form2 = new Form2();
                        form2.MdiParent = parent;
                        form2.loadData(important[idx]);
                        form2.index = idx;
                        form2.StartPosition = FormStartPosition.CenterScreen;
                        form2.frmData.isNowOpen = true;
                        form2.Show();
                    }
                    break;
                case "To-do-list":
                    if (hasChild)
                    {
                        for (int i = 0; i < childFormArray.Length; i++)
                        {
                            Form2 current = childFormArray[i] as Form2;
                            if (current.index == idx && current.frmData.isToDoList)
                            {
                                isOpened = true;
                                current.Focus();
                                break;
                            }
                        }
                    }

                    if (!isOpened)
                    {
                        Form2 form2 = new Form2();
                        form2.MdiParent = parent;
                        form2.loadData(todoList[idx]);
                        form2.index = idx;
                        form2.frmData.isNowOpen = true;
                        form2.Show();
                    }
                    break;
            }
        }

        private void textBoxSearch_Enter(object sender, EventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            int visibleTime = 1500;

            ToolTip toolTip = new ToolTip();
            toolTip.Show("seperate words by space character", txtBox, 0, 20, visibleTime);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
