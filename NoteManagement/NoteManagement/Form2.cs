﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Form1 parent = this.Owner as Form1;
            parent.config.tagStr = this.textBoxTag.Text;
            parent.Text = this.textBoxDescription.Text;

            var button = parent.toolStrip.Items.Find("toolStripButtonTimer", true);
            if (radioButtonReminder.Checked == true)
            {
                button[0].Visible = true;

                parent.Icon = Icon.FromHandle(Properties.Resources.reminder.GetHicon());
                parent.ShowIcon = true;
            }
            else if (radioButtonToDoList.Checked == true)
            {
                button[0].Visible = false;

                parent.Icon = Icon.FromHandle(Properties.Resources.todolist.GetHicon());
                parent.ShowIcon = true;
            }
            else if (radioButtonNone.Checked == true) 
            {
                button[0].Visible = false;

                parent.Icon = Icon.FromHandle(Properties.Resources.none.GetHicon());
                parent.ShowIcon = true;
            }
            else
            {
                button[0].Visible = false;
                parent.config.important = true;

                parent.Icon = Icon.FromHandle(Properties.Resources.important.GetHicon());
                parent.ShowIcon = true;
            }

            this.Close();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Form1 parent = this.Owner as Form1;
            this.textBoxDescription.Text = parent.Text;
            this.textBoxTag.Text = parent.config.tagStr;
        }
    }
}
