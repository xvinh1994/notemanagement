﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        internal DataNote frmData = new DataNote();
        public int index = 0;

        public void loadData(DataNote data)
        {
            frmData = data;
            this.richTextBox.Text = frmData.dataInNote;
            this.Text = frmData.description;

            if (frmData.isEverything)
                this.Icon = Icon.FromHandle(Properties.Resources.none.GetHicon());
            else if (frmData.isImportant)
            {
                this.Icon = Icon.FromHandle(Properties.Resources.important.GetHicon());
                this.StartPosition = FormStartPosition.CenterScreen;
            }
            else if (frmData.isToDoList)
                this.Icon = Icon.FromHandle(Properties.Resources.todolist.GetHicon());
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.frmData.isNowOpen = false;
            Form1 parent = this.MdiParent as Form1;
            parent.triggerSaveForChild();
        }

        private void sendThisNoteByMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form6 form6 = new Form6(frmData);
            form6.ShowDialog();
        }
    }
}
