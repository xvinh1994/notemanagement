﻿namespace NoteManagement
{
    partial class SendMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAccount = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelReciever = new System.Windows.Forms.Label();
            this.textBoxAccount = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxReceiver = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.radioButtonYahoo = new System.Windows.Forms.RadioButton();
            this.radioButtonGmail = new System.Windows.Forms.RadioButton();
            this.radioButtonHotmail = new System.Windows.Forms.RadioButton();
            this.labelServer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelAccount
            // 
            this.labelAccount.AutoSize = true;
            this.labelAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelAccount.Location = new System.Drawing.Point(12, 44);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(53, 15);
            this.labelAccount.TabIndex = 0;
            this.labelAccount.Text = "Account:";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelPassword.Location = new System.Drawing.Point(12, 77);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(64, 15);
            this.labelPassword.TabIndex = 1;
            this.labelPassword.Text = "Password:";
            // 
            // labelReciever
            // 
            this.labelReciever.AutoSize = true;
            this.labelReciever.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelReciever.Location = new System.Drawing.Point(12, 111);
            this.labelReciever.Name = "labelReciever";
            this.labelReciever.Size = new System.Drawing.Size(58, 15);
            this.labelReciever.TabIndex = 2;
            this.labelReciever.Text = "Receiver:";
            // 
            // textBoxAccount
            // 
            this.textBoxAccount.Enabled = false;
            this.textBoxAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBoxAccount.Location = new System.Drawing.Point(89, 43);
            this.textBoxAccount.Name = "textBoxAccount";
            this.textBoxAccount.Size = new System.Drawing.Size(336, 23);
            this.textBoxAccount.TabIndex = 3;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Enabled = false;
            this.textBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBoxPassword.Location = new System.Drawing.Point(89, 77);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(336, 23);
            this.textBoxPassword.TabIndex = 4;
            // 
            // textBoxReceiver
            // 
            this.textBoxReceiver.Enabled = false;
            this.textBoxReceiver.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBoxReceiver.Location = new System.Drawing.Point(89, 110);
            this.textBoxReceiver.Name = "textBoxReceiver";
            this.textBoxReceiver.Size = new System.Drawing.Size(336, 23);
            this.textBoxReceiver.TabIndex = 5;
            // 
            // buttonSend
            // 
            this.buttonSend.Enabled = false;
            this.buttonSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.buttonSend.Location = new System.Drawing.Point(189, 151);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 31);
            this.buttonSend.TabIndex = 6;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // radioButtonYahoo
            // 
            this.radioButtonYahoo.AutoSize = true;
            this.radioButtonYahoo.Location = new System.Drawing.Point(80, 14);
            this.radioButtonYahoo.Name = "radioButtonYahoo";
            this.radioButtonYahoo.Size = new System.Drawing.Size(78, 17);
            this.radioButtonYahoo.TabIndex = 7;
            this.radioButtonYahoo.TabStop = true;
            this.radioButtonYahoo.Text = "Yahoo Mail";
            this.radioButtonYahoo.UseVisualStyleBackColor = true;
            this.radioButtonYahoo.CheckedChanged += new System.EventHandler(this.radioButtonYahoo_CheckedChanged);
            // 
            // radioButtonGmail
            // 
            this.radioButtonGmail.AutoSize = true;
            this.radioButtonGmail.Location = new System.Drawing.Point(213, 14);
            this.radioButtonGmail.Name = "radioButtonGmail";
            this.radioButtonGmail.Size = new System.Drawing.Size(51, 17);
            this.radioButtonGmail.TabIndex = 8;
            this.radioButtonGmail.TabStop = true;
            this.radioButtonGmail.Text = "Gmail";
            this.radioButtonGmail.UseVisualStyleBackColor = true;
            this.radioButtonGmail.CheckedChanged += new System.EventHandler(this.radioButtonGmail_CheckedChanged);
            // 
            // radioButtonHotmail
            // 
            this.radioButtonHotmail.AutoSize = true;
            this.radioButtonHotmail.Location = new System.Drawing.Point(340, 14);
            this.radioButtonHotmail.Name = "radioButtonHotmail";
            this.radioButtonHotmail.Size = new System.Drawing.Size(60, 17);
            this.radioButtonHotmail.TabIndex = 9;
            this.radioButtonHotmail.TabStop = true;
            this.radioButtonHotmail.Text = "Hotmail";
            this.radioButtonHotmail.UseVisualStyleBackColor = true;
            this.radioButtonHotmail.CheckedChanged += new System.EventHandler(this.radioButtonHotmail_CheckedChanged);
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelServer.Location = new System.Drawing.Point(12, 14);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(42, 15);
            this.labelServer.TabIndex = 10;
            this.labelServer.Text = "Server";
            // 
            // SendMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 194);
            this.Controls.Add(this.labelServer);
            this.Controls.Add(this.radioButtonHotmail);
            this.Controls.Add(this.radioButtonGmail);
            this.Controls.Add(this.radioButtonYahoo);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxReceiver);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxAccount);
            this.Controls.Add(this.labelReciever);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelAccount);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SendMail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Send Your Note";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelReciever;
        private System.Windows.Forms.TextBox textBoxAccount;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxReceiver;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.RadioButton radioButtonYahoo;
        private System.Windows.Forms.RadioButton radioButtonGmail;
        private System.Windows.Forms.RadioButton radioButtonHotmail;
        private System.Windows.Forms.Label labelServer;
    }
}