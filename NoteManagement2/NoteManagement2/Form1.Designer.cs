﻿namespace NoteManagement2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveAllNotes = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMailAllNotes = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAbout = new System.Windows.Forms.ToolStripButton();
            this.treeView = new System.Windows.Forms.TreeView();
            this.contextMenuStripTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllNoteInCatergoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllNoteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timerAutoSave = new System.Windows.Forms.Timer(this.components);
            this.timerCountDown = new System.Windows.Forms.Timer(this.components);
            this.toolStrip.SuspendLayout();
            this.contextMenuStripTree.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNew,
            this.toolStripButtonSaveAllNotes,
            this.toolStripButtonSearch,
            this.toolStripButtonMailAllNotes,
            this.toolStripButtonAbout});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(661, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonNew
            // 
            this.toolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNew.Image = global::NoteManagement2.Properties.Resources._new;
            this.toolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNew.Name = "toolStripButtonNew";
            this.toolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNew.Text = "New";
            this.toolStripButtonNew.Click += new System.EventHandler(this.toolStripButtonNew_Click);
            // 
            // toolStripButtonSaveAllNotes
            // 
            this.toolStripButtonSaveAllNotes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveAllNotes.Image = global::NoteManagement2.Properties.Resources.save;
            this.toolStripButtonSaveAllNotes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveAllNotes.Name = "toolStripButtonSaveAllNotes";
            this.toolStripButtonSaveAllNotes.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveAllNotes.Text = "Save All Notes";
            this.toolStripButtonSaveAllNotes.Click += new System.EventHandler(this.toolStripButtonSaveAllNotes_Click);
            // 
            // toolStripButtonSearch
            // 
            this.toolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSearch.Image = global::NoteManagement2.Properties.Resources.search;
            this.toolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSearch.Name = "toolStripButtonSearch";
            this.toolStripButtonSearch.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSearch.Text = "Search Notes";
            this.toolStripButtonSearch.Click += new System.EventHandler(this.toolStripButtonSearch_Click);
            // 
            // toolStripButtonMailAllNotes
            // 
            this.toolStripButtonMailAllNotes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonMailAllNotes.Image = global::NoteManagement2.Properties.Resources.Gmail;
            this.toolStripButtonMailAllNotes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMailAllNotes.Name = "toolStripButtonMailAllNotes";
            this.toolStripButtonMailAllNotes.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonMailAllNotes.Text = "Mail All Notes";
            this.toolStripButtonMailAllNotes.Click += new System.EventHandler(this.toolStripButtonMailAllNotes_Click);
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAbout.Image = global::NoteManagement2.Properties.Resources.about;
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAbout.Text = "About";
            this.toolStripButtonAbout.Click += new System.EventHandler(this.toolStripButtonAbout_Click);
            // 
            // treeView
            // 
            this.treeView.ContextMenuStrip = this.contextMenuStripTree;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView.Location = new System.Drawing.Point(0, 25);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(143, 320);
            this.treeView.TabIndex = 2;
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            this.treeView.DoubleClick += new System.EventHandler(this.treeView_DoubleClick);
            // 
            // contextMenuStripTree
            // 
            this.contextMenuStripTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.deleteAllNoteInCatergoryToolStripMenuItem,
            this.deleteAllNoteToolStripMenuItem1});
            this.contextMenuStripTree.Name = "contextMenuStripTree";
            this.contextMenuStripTree.Size = new System.Drawing.Size(222, 70);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = global::NoteManagement2.Properties.Resources.delete;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.deleteToolStripMenuItem.Text = "Delete this Note";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // deleteAllNoteInCatergoryToolStripMenuItem
            // 
            this.deleteAllNoteInCatergoryToolStripMenuItem.Image = global::NoteManagement2.Properties.Resources.delete;
            this.deleteAllNoteInCatergoryToolStripMenuItem.Name = "deleteAllNoteInCatergoryToolStripMenuItem";
            this.deleteAllNoteInCatergoryToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.deleteAllNoteInCatergoryToolStripMenuItem.Text = "Delete All Note in Catergory";
            this.deleteAllNoteInCatergoryToolStripMenuItem.Click += new System.EventHandler(this.deleteAllNoteInCatergoryToolStripMenuItem_Click);
            // 
            // deleteAllNoteToolStripMenuItem1
            // 
            this.deleteAllNoteToolStripMenuItem1.Image = global::NoteManagement2.Properties.Resources.delete;
            this.deleteAllNoteToolStripMenuItem1.Name = "deleteAllNoteToolStripMenuItem1";
            this.deleteAllNoteToolStripMenuItem1.Size = new System.Drawing.Size(221, 22);
            this.deleteAllNoteToolStripMenuItem1.Text = "Delete All Notes";
            this.deleteAllNoteToolStripMenuItem1.Click += new System.EventHandler(this.deleteAllNoteToolStripMenuItem1_Click);
            // 
            // timerAutoSave
            // 
            this.timerAutoSave.Interval = 60000;
            this.timerAutoSave.Tick += new System.EventHandler(this.timerAutoSave_Tick);
            // 
            // timerCountDown
            // 
            this.timerCountDown.Interval = 1000;
            this.timerCountDown.Tick += new System.EventHandler(this.timerCountDown_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 345);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.toolStrip);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Text = "Note Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.contextMenuStripTree.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ToolStripButton toolStripButtonNew;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTree;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAllNoteInCatergoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAllNoteToolStripMenuItem1;
        private System.Windows.Forms.Timer timerAutoSave;
        private System.Windows.Forms.Timer timerCountDown;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveAllNotes;
        private System.Windows.Forms.ToolStripButton toolStripButtonSearch;
        private System.Windows.Forms.ToolStripButton toolStripButtonAbout;
        private System.Windows.Forms.ToolStripButton toolStripButtonMailAllNotes;
    }
}

