﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteManagement2
{
    public class DataNote
    {
        public bool isImportant { get; set; }
        public bool isToDoList { get; set; }
        public bool isEverything { get; set; }
        public bool isAlarmOn { get; set; }
        public bool isNowOpen { get; set; }

        public string dataInNote { get; set; }
        public string tag { get; set; }
        public string description { get; set; }

        public int timeLeft { get; set; }

        public DataNote()
        {
            isImportant = false;
            isToDoList = false;
            isEverything = false;
            isAlarmOn = false;
            isNowOpen = false;

            dataInNote = "";
            tag = "";
            description = "";
            timeLeft = 0;
        }

        public DataNote(string tag, string description, string data, string important, string todolist, 
            string everything, string alarmOn, string timeLeft)
        {
            this.tag = tag;
            this.description = description;
            this.dataInNote = data;
            this.isImportant = Convert.ToBoolean(important);
            this.isToDoList = Convert.ToBoolean(todolist);
            this.isEverything = Convert.ToBoolean(everything);
            this.isAlarmOn = Convert.ToBoolean(alarmOn);
            this.timeLeft = Convert.ToInt32(timeLeft);
            this.isNowOpen = false;
        }
    }
}
