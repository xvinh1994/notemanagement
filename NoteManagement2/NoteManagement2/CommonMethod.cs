﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace NoteManagement2
{
    class CommonMethod
    {
        public static void writeXMLFile(List<DataNote> todoList, List<DataNote> important, List<DataNote> everything)
        {
            if (!File.Exists("data.xml"))
            {
                newXMLFileCreate();
            }
            else
            {
                File.Delete("data.xml");
                newXMLFileCreate();
            }

            foreach (var note in todoList)
            {
                if (note.dataInNote != "") 
                    writeData(note);
            }
            
            foreach (var note in important)
            {
                if (note.dataInNote != "")
                    writeData(note);
            }

            foreach (var note in everything)
            {
                if (note.dataInNote != "")
                    writeData(note);
            }
        }

        private static void writeData(DataNote note)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("data.xml");

            string data = note.dataInNote;
            XmlNode newNote = createNewNode(note.tag, note.description, note.isImportant.ToString(), note.isToDoList.ToString(),
            note.isEverything.ToString(), note.isAlarmOn.ToString(), note.timeLeft.ToString(), data, doc);

            doc.DocumentElement.AppendChild(newNote);
            doc.Save("data.xml");
        }

        private static XmlNode createNewNode(string tag, string description, string important, string todolist, string everything, 
            string alarmOn, string timeleft, string data, XmlDocument doc)
        {
            XmlNode nodeNote = doc.CreateElement("Note");

            XmlNode nodeTag = doc.CreateElement("tag");
            nodeTag.InnerText = tag;
            nodeNote.AppendChild(nodeTag);

            XmlNode nodeImportant = doc.CreateElement("important");
            nodeImportant.InnerText = important;
            nodeNote.AppendChild(nodeImportant);

            XmlNode nodeToDoList = doc.CreateElement("todolist");
            nodeToDoList.InnerText = todolist;
            nodeNote.AppendChild(nodeToDoList);

            XmlNode nodeEverything = doc.CreateElement("everything");
            nodeEverything.InnerText = everything;
            nodeNote.AppendChild(nodeEverything);

            XmlNode nodeAlarm = doc.CreateElement("alarm");
            nodeAlarm.InnerText = alarmOn;
            nodeNote.AppendChild(nodeAlarm);

            XmlNode nodeTimeleft = doc.CreateElement("timeleft");
            nodeTimeleft.InnerText = timeleft;
            nodeNote.AppendChild(nodeTimeleft);

            XmlNode nodeDescription = doc.CreateElement("description");
            nodeDescription.InnerText = description;
            nodeNote.AppendChild(nodeDescription);

            XmlNode nodeData = doc.CreateElement("data");
            nodeData.InnerText += data;
            nodeNote.AppendChild(nodeData);


            return nodeNote;
        }

        private static void newXMLFileCreate()
        {
            StreamWriter mywrite = new StreamWriter("data.xml");

            string data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Notes>\n\n</Notes>";
            mywrite.Write(data);

            mywrite.Close();
        }

        public static void loadSavedNotes(ref List<DataNote> todoList, ref List<DataNote> important, ref List<DataNote> everything)
        {
            List<DataNote> allSavedNotes = readXMLFile();

            foreach (var note in allSavedNotes)
            {
                if (note.isEverything) everything.Add(note);
                else if (note.isImportant) important.Add(note);
                else if (note.isToDoList) todoList.Add(note);
            }
        }

        private static List<DataNote> readXMLFile()
        {
            List<DataNote> result = new List<DataNote>();
            if (File.Exists("data.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("data.xml");

                XmlNode root = doc.SelectSingleNode("Notes");

                foreach (XmlNode node in root.ChildNodes)
                {
                    string tag = node.SelectSingleNode("tag").InnerText;
                    string timeleft = node.SelectSingleNode("timeleft").InnerText;
                    string important = node.SelectSingleNode("important").InnerText;
                    string todolist = node.SelectSingleNode("todolist").InnerText;
                    string everything = node.SelectSingleNode("everything").InnerText;
                    string alarmOn = node.SelectSingleNode("alarm").InnerText;
                    string data = node.SelectSingleNode("data").InnerText;
                    string description = node.SelectSingleNode("description").InnerText;

                    DataNote dataNote = new DataNote(tag, description, data, important, todolist, everything, alarmOn, timeleft);
                    result.Add(dataNote);
                }
            }

            return result;
        }
    }
}
