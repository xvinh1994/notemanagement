﻿namespace NoteManagement2
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonYahoo = new System.Windows.Forms.RadioButton();
            this.radioButtonGmail = new System.Windows.Forms.RadioButton();
            this.radioButtonHotmail = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxYourAccount = new System.Windows.Forms.TextBox();
            this.textBoxYourPassword = new System.Windows.Forms.TextBox();
            this.textBoxSendTo = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mail Server:";
            // 
            // radioButtonYahoo
            // 
            this.radioButtonYahoo.AutoSize = true;
            this.radioButtonYahoo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonYahoo.Location = new System.Drawing.Point(115, 9);
            this.radioButtonYahoo.Name = "radioButtonYahoo";
            this.radioButtonYahoo.Size = new System.Drawing.Size(90, 19);
            this.radioButtonYahoo.TabIndex = 1;
            this.radioButtonYahoo.Text = "Yahoo! Mail";
            this.radioButtonYahoo.UseVisualStyleBackColor = true;
            this.radioButtonYahoo.CheckedChanged += new System.EventHandler(this.radioButtonYahoo_CheckedChanged);
            // 
            // radioButtonGmail
            // 
            this.radioButtonGmail.AutoSize = true;
            this.radioButtonGmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonGmail.Location = new System.Drawing.Point(264, 9);
            this.radioButtonGmail.Name = "radioButtonGmail";
            this.radioButtonGmail.Size = new System.Drawing.Size(58, 19);
            this.radioButtonGmail.TabIndex = 2;
            this.radioButtonGmail.Text = "Gmail";
            this.radioButtonGmail.UseVisualStyleBackColor = true;
            this.radioButtonGmail.CheckedChanged += new System.EventHandler(this.radioButtonGmail_CheckedChanged);
            // 
            // radioButtonHotmail
            // 
            this.radioButtonHotmail.AutoSize = true;
            this.radioButtonHotmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.radioButtonHotmail.Location = new System.Drawing.Point(401, 9);
            this.radioButtonHotmail.Name = "radioButtonHotmail";
            this.radioButtonHotmail.Size = new System.Drawing.Size(68, 19);
            this.radioButtonHotmail.TabIndex = 3;
            this.radioButtonHotmail.Text = "Hotmail";
            this.radioButtonHotmail.UseVisualStyleBackColor = true;
            this.radioButtonHotmail.CheckedChanged += new System.EventHandler(this.radioButtonHotmail_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(24, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Your mail account:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(24, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Your password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(24, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Send to:";
            // 
            // textBoxYourAccount
            // 
            this.textBoxYourAccount.Enabled = false;
            this.textBoxYourAccount.Location = new System.Drawing.Point(155, 52);
            this.textBoxYourAccount.Name = "textBoxYourAccount";
            this.textBoxYourAccount.Size = new System.Drawing.Size(339, 20);
            this.textBoxYourAccount.TabIndex = 7;
            // 
            // textBoxYourPassword
            // 
            this.textBoxYourPassword.Enabled = false;
            this.textBoxYourPassword.Location = new System.Drawing.Point(155, 90);
            this.textBoxYourPassword.Name = "textBoxYourPassword";
            this.textBoxYourPassword.PasswordChar = '*';
            this.textBoxYourPassword.Size = new System.Drawing.Size(339, 20);
            this.textBoxYourPassword.TabIndex = 8;
            // 
            // textBoxSendTo
            // 
            this.textBoxSendTo.Enabled = false;
            this.textBoxSendTo.Location = new System.Drawing.Point(155, 132);
            this.textBoxSendTo.Name = "textBoxSendTo";
            this.textBoxSendTo.Size = new System.Drawing.Size(339, 20);
            this.textBoxSendTo.TabIndex = 9;
            // 
            // buttonSend
            // 
            this.buttonSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.buttonSend.Location = new System.Drawing.Point(205, 186);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(89, 40);
            this.buttonSend.TabIndex = 10;
            this.buttonSend.Text = "SEND";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 238);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxSendTo);
            this.Controls.Add(this.textBoxYourPassword);
            this.Controls.Add(this.textBoxYourAccount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButtonHotmail);
            this.Controls.Add(this.radioButtonGmail);
            this.Controls.Add(this.radioButtonYahoo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form6";
            this.Text = "Share Your Notes";
            this.Load += new System.EventHandler(this.Form6_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonYahoo;
        private System.Windows.Forms.RadioButton radioButtonGmail;
        private System.Windows.Forms.RadioButton radioButtonHotmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxYourAccount;
        private System.Windows.Forms.TextBox textBoxYourPassword;
        private System.Windows.Forms.TextBox textBoxSendTo;
        private System.Windows.Forms.Button buttonSend;
    }
}