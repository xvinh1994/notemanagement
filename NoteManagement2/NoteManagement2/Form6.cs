﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteManagement2
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        List<DataNote> dataToSend = new List<DataNote>();

        public Form6(DataNote data)
        {
            InitializeComponent();
            dataToSend.Add(data);
        }

        public Form6(List<DataNote> dataList)
        {
            InitializeComponent();
            dataToSend = dataList;
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.FromHandle(Properties.Resources.Gmail.GetHicon());
        }

        private void radioButtonYahoo_CheckedChanged(object sender, EventArgs e)
        {
            textBoxYourAccount.Enabled = radioButtonYahoo.Checked;
            textBoxYourPassword.Enabled = radioButtonYahoo.Checked;
            textBoxSendTo.Enabled = radioButtonYahoo.Checked;
            buttonSend.Enabled = radioButtonYahoo.Checked;
        }

        private void radioButtonGmail_CheckedChanged(object sender, EventArgs e)
        {
            textBoxYourAccount.Enabled = radioButtonGmail.Checked;
            textBoxYourPassword.Enabled = radioButtonGmail.Checked;
            textBoxSendTo.Enabled = radioButtonGmail.Checked;
            buttonSend.Enabled = radioButtonGmail.Checked;
        }

        private void radioButtonHotmail_CheckedChanged(object sender, EventArgs e)
        {
            textBoxYourAccount.Enabled = radioButtonHotmail.Checked;
            textBoxYourPassword.Enabled = radioButtonHotmail.Checked;
            textBoxSendTo.Enabled = radioButtonHotmail.Checked;
            buttonSend.Enabled = radioButtonHotmail.Checked;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (dataToSend.Count > 0)
            {
                MessageBox.Show("Note will be sent to " + textBoxSendTo.Text + " with subject: \"My Note Sharing\"");
                MailMessage mail;
                SmtpClient client;
                
                string data = "";
                for (int i=0; i<dataToSend.Count; i++)
                { 
                    data += "- Tag: [" + dataToSend[i].tag + "]\n" +
                        "- Description: " + dataToSend[i].description + "\n" +
                        "- Data in note: " + dataToSend[i].dataInNote + "\n\n";
                }
                data +="- Sending Day: " + DateTime.Now.ToString();

                string server = "";
                string sendUser = "";
                string passwordUser = "";
                string receiveUser = "";

                if (radioButtonYahoo.Checked)
                {
                    server = "smtp.mail.yahoo.com";
                }
                else if (radioButtonGmail.Checked)
                {
                    server = "smtp.gmail.com";
                }
                else if (radioButtonHotmail.Checked)
                {
                    server = "smtp.live.com";
                }

                sendUser = textBoxYourAccount.Text;
                passwordUser = textBoxYourPassword.Text;
                receiveUser = textBoxSendTo.Text;

                mail = new MailMessage(sendUser, receiveUser, "Share Note by mail", data);
                client = new SmtpClient(server);
                client.Port = 587;
                client.Credentials = new System.Net.NetworkCredential(sendUser, passwordUser);
                client.EnableSsl = true;
                try
                {
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else MessageBox.Show("No data in note to send");
            this.Close();
        }
    }
}
