﻿namespace NoteManagement
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTag = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxTag = new System.Windows.Forms.TextBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.radioButtonToDoList = new System.Windows.Forms.RadioButton();
            this.radioButtonReminder = new System.Windows.Forms.RadioButton();
            this.radioButtonNone = new System.Windows.Forms.RadioButton();
            this.radioButtonImportant = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // labelTag
            // 
            this.labelTag.AutoSize = true;
            this.labelTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelTag.Location = new System.Drawing.Point(25, 16);
            this.labelTag.Name = "labelTag";
            this.labelTag.Size = new System.Drawing.Size(37, 17);
            this.labelTag.TabIndex = 0;
            this.labelTag.Text = "Tag:";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelDescription.Location = new System.Drawing.Point(25, 56);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(83, 17);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "Description:";
            // 
            // textBoxTag
            // 
            this.textBoxTag.Location = new System.Drawing.Point(121, 15);
            this.textBoxTag.Name = "textBoxTag";
            this.textBoxTag.Size = new System.Drawing.Size(293, 20);
            this.textBoxTag.TabIndex = 2;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(121, 55);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(293, 20);
            this.textBoxDescription.TabIndex = 3;
            // 
            // buttonOK
            // 
            this.buttonOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.buttonOK.Location = new System.Drawing.Point(141, 124);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(172, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "Set Tag and Description";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // radioButtonToDoList
            // 
            this.radioButtonToDoList.AutoSize = true;
            this.radioButtonToDoList.Location = new System.Drawing.Point(28, 98);
            this.radioButtonToDoList.Name = "radioButtonToDoList";
            this.radioButtonToDoList.Size = new System.Drawing.Size(74, 17);
            this.radioButtonToDoList.TabIndex = 5;
            this.radioButtonToDoList.Text = "To-Do-List";
            this.radioButtonToDoList.UseVisualStyleBackColor = true;
            // 
            // radioButtonReminder
            // 
            this.radioButtonReminder.AutoSize = true;
            this.radioButtonReminder.Location = new System.Drawing.Point(141, 98);
            this.radioButtonReminder.Name = "radioButtonReminder";
            this.radioButtonReminder.Size = new System.Drawing.Size(70, 17);
            this.radioButtonReminder.TabIndex = 6;
            this.radioButtonReminder.Text = "Reminder";
            this.radioButtonReminder.UseVisualStyleBackColor = true;
            // 
            // radioButtonNone
            // 
            this.radioButtonNone.AutoSize = true;
            this.radioButtonNone.Checked = true;
            this.radioButtonNone.Location = new System.Drawing.Point(363, 98);
            this.radioButtonNone.Name = "radioButtonNone";
            this.radioButtonNone.Size = new System.Drawing.Size(51, 17);
            this.radioButtonNone.TabIndex = 7;
            this.radioButtonNone.TabStop = true;
            this.radioButtonNone.Text = "None";
            this.radioButtonNone.UseVisualStyleBackColor = true;
            // 
            // radioButtonImportant
            // 
            this.radioButtonImportant.AutoSize = true;
            this.radioButtonImportant.Location = new System.Drawing.Point(252, 98);
            this.radioButtonImportant.Name = "radioButtonImportant";
            this.radioButtonImportant.Size = new System.Drawing.Size(69, 17);
            this.radioButtonImportant.TabIndex = 8;
            this.radioButtonImportant.Text = "Important";
            this.radioButtonImportant.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 159);
            this.Controls.Add(this.radioButtonImportant);
            this.Controls.Add(this.radioButtonNone);
            this.Controls.Add(this.radioButtonReminder);
            this.Controls.Add(this.radioButtonToDoList);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.textBoxTag);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelTag);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tag and Description";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTag;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxTag;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.RadioButton radioButtonToDoList;
        private System.Windows.Forms.RadioButton radioButtonReminder;
        private System.Windows.Forms.RadioButton radioButtonNone;
        private System.Windows.Forms.RadioButton radioButtonImportant;
    }
}