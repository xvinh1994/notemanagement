﻿using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace NoteManagement
{
    public partial class Form1 : Form
    {
        public List<Form1> listNote = new List<Form1>();
        public int index = 0;
        public NoteConfig config = new NoteConfig();

        public Form1()
        {
            InitializeComponent();

            listNote.Add(this);
            index = listNote.Count - 1;

            if (readXML())
            {
                for (int i = 1; i < listNote.Count; i++)
                {
                    listNote[i].listNote = this.listNote;
                }
            }
        }

        public Form1(string data, string description, NoteConfig fileConfig)
        {
            InitializeComponent();

            this.richTextBoxNote.Text = data;
            this.Text = description;
            this.config.tagStr = fileConfig.tagStr;
            this.config.timeLeft = fileConfig.timeLeft;
            this.config.important = fileConfig.important;
            this.richTextBoxNote.BackColor = Color.FromArgb(fileConfig.R, fileConfig.G, fileConfig.B);

            listNote.Add(this);
            index = listNote.Count - 1;

            if (this.config.important == true) this.StartPosition = FormStartPosition.CenterScreen;
            if (this.config.timeLeft > 0)
            {
                this.StartPosition = FormStartPosition.CenterScreen;
                this.timerReminder.Start();
            }

            this.ShowInTaskbar = false;
            this.Show();
        }

        public Form1(ref List<Form1> list)
        {
            InitializeComponent();

            list.Add(this);
            listNote = list;
            index = list.Count - 1;
        }

        private bool readXML()
        {
            if (File.Exists("data.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("data.xml");

                XmlNode root = doc.SelectSingleNode("Notes");

                foreach (XmlNode node in root.ChildNodes)
                {
                    string tag = node.SelectSingleNode("tag").InnerText;
                    string timeleft = node.SelectSingleNode("timeleft").InnerText;
                    string important = node.SelectSingleNode("important").InnerText;
                    string R = node.SelectSingleNode("red").InnerText;
                    string B = node.SelectSingleNode("blue").InnerText;
                    string G = node.SelectSingleNode("green").InnerText;
                    string data = node.SelectSingleNode("data").InnerText;
                    string description = node.SelectSingleNode("description").InnerText;

                    NoteConfig fileConfig = new NoteConfig(tag, timeleft, important, R, B, G);
                    this.listNote.Add(new Form1(data, description, fileConfig));
                }

                if (this.listNote.Count != 0)
                    return true;
                else return false;
            }
            return false;
        }

        private void toolStripButtonTag_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Owner = this;
            form2.ShowDialog();

            writeToXML();
        }

        private void toolStripButtonNew_Click(object sender, EventArgs e)
        {
            Form1 newForm = new Form1(ref listNote);
            newForm.ShowInTaskbar = false;
            newForm.Show();

            Form2 form2 = new Form2();
            form2.Owner = newForm;
            form2.ShowDialog();
        }
        
        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(listNote);
            form3.Show();
        }

        private bool confirmToDo(string msg)
        {
            if (MessageBox.Show(msg, "Confirmation", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
                == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            writeToXML();
        }

        private void writeToXML()
        {
            if (!File.Exists("data.xml"))
            {
                newXMLFileCreate();
            }
            else
            {
                File.Delete("data.xml");
                newXMLFileCreate();

                XmlDocument doc = new XmlDocument();
                doc.Load("data.xml");

                foreach (var note in listNote)
                {
                    if (note.richTextBoxNote.Text != "" && !note.config.isThisDeleted)
                    {
                        string data = note.richTextBoxNote.Text;
                        XmlNode newNote = createNewNode(note.config.tagStr, note.config.description, note.config.important.ToString(), note.config.timeLeft.ToString(),
                            note.config.R.ToString(), note.config.B.ToString(), note.config.G.ToString(), data, doc);

                        doc.DocumentElement.AppendChild(newNote);
                        doc.Save("data.xml");
                    }
                }
            }
        }

        private void newXMLFileCreate()
        {
            StreamWriter mywrite = new StreamWriter("data.xml");

            string data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Notes>\n\n</Notes>";
            mywrite.Write(data);

            mywrite.Close();
        }

        private XmlNode createNewNode(string tag, string description, string important, string timeleft, string R, string B, string G, 
            string data, XmlDocument doc)
        {
            XmlNode nodeNote = doc.CreateElement("Note");

            XmlNode nodeTag = doc.CreateElement("tag");
            nodeTag.InnerText = tag;
            nodeNote.AppendChild(nodeTag);

            XmlNode nodeImportant = doc.CreateElement("important");
            nodeImportant.InnerText = important;
            nodeNote.AppendChild(nodeImportant);

            XmlNode nodeTimeleft = doc.CreateElement("timeleft");
            nodeTimeleft.InnerText = timeleft;
            nodeNote.AppendChild(nodeTimeleft);

            XmlNode nodeR = doc.CreateElement("red");
            nodeR.InnerText = R;
            nodeNote.AppendChild(nodeR);

            XmlNode nodeB = doc.CreateElement("blue");
            nodeB.InnerText = B;
            nodeNote.AppendChild(nodeB);

            XmlNode nodeG = doc.CreateElement("green");
            nodeG.InnerText = G;
            nodeNote.AppendChild(nodeG);

            XmlNode nodeDescription = doc.CreateElement("description");
            nodeDescription.InnerText = description;
            nodeNote.AppendChild(nodeDescription);

            XmlNode nodeData = doc.CreateElement("data");
            nodeData.InnerText += data;
            nodeNote.AppendChild(nodeData);


            return nodeNote;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeToXML();
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timerAutoSave.Start();

            this.Icon = Icon.FromHandle(Properties.Resources.none.GetHicon());

            this.config.R = richTextBoxNote.BackColor.R;
            this.config.B = richTextBoxNote.BackColor.B;
            this.config.G = richTextBoxNote.BackColor.G;
        }

        private void timerAutoSave_Tick(object sender, EventArgs e)
        {
            writeToXML();
        }

        private void toolStripButtonAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Features:\r\n - New, update, delete, save note.\r\n " + 
                "- Reminder with sound.\r\n - Autosave after 1 minute.\r\n " +
                "- Important & Reminder notes will be on center screen\r\n " +
                "- Share your chosen note by mail\r\n " +
                "- Change your note color\r\n " +
                "Instruction:\r\n - Activate \"Reminder\" option by choosing \"Tag - Description\" button.\r\n " +
                "- See your information of note in Tag & Description\r\n " + 
                "- Update Tag or Description in \"Tag - Description\" button.\r\n " +
                "- Close one note, another notes will be closed and saved.\r\n " +
                "- Change color by right click on note");
        }

        private void toolStripButtonTimer_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            form4.Owner = this;
            form4.ShowDialog();

            if (this.config.timeLeft != 0) timerReminder.Start();
            else timerReminder.Stop();
        }

        private void timerReminder_Tick(object sender, EventArgs e)
        {
            this.config.timeLeft--;

            if (this.config.timeLeft == 0)
            {
                SoundPlayer soundReminder = new SoundPlayer();
                soundReminder.Stream = Properties.Resources.ringring;
                soundReminder.Play();

                timerReminder.Stop();

                MessageBox.Show("Time to do things in this note!!!!!!!!");
                this.Focus();
            }
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (confirmToDo("Delete this note?"))
            {
                config.isThisDeleted = true;

                writeToXML();

                listNote[(this.index + 1) % listNote.Count].ShowInTaskbar = true;

                if (this.index == 0)
                {
                    if (this.listNote.Count == 1) this.Close();
                    else this.Hide();
                }
                else this.Close();
            }
        }

        private void emailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendMail sendForm = new SendMail();
            sendForm.Owner = this;
            sendForm.ShowDialog();
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(197, 228, 246);
            toolStrip.BackColor = Color.FromArgb(197, 228, 246);
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(192, 242, 188);
            toolStrip.BackColor = Color.FromArgb(192, 242, 188);
        }

        private void pinkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(239, 190, 239);
            toolStrip.BackColor = Color.FromArgb(239, 190, 239);
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(207, 195, 254);
            toolStrip.BackColor = Color.FromArgb(207, 195, 254);
        }

        private void whiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(243, 243, 243);
            toolStrip.BackColor = Color.FromArgb(243, 243, 243);
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxNote.BackColor = Color.FromArgb(248, 248, 88);
            toolStrip.BackColor = Color.FromArgb(248, 248, 88);
        }
    }
}
